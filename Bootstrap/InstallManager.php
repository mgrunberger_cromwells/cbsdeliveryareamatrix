<?php

namespace CBSCheckout\Bootstrap;

use CBSCheckout\Models\DeliverySlot;
use CBSCheckout\Models\DeliverySlotUsage;
use CBSCheckout\Models\DeliveryZone;
use CBSCheckout\Models\TransferLeadTime;
use Doctrine\ORM\Tools\SchemaTool;
use Shopware\Bundle\AttributeBundle\Service\CrudService;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\Context\InstallContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Shopware\Models\Plugin\Plugin;

class InstallManager
{
    /** @var ContainerInterface  */
    private $container;

    /** @var InstallContext $context */
    private $context;

    /** @var ModelManager */
    private $crudService;

    /** @var CrudService */
    private $modelManager;

    /* @var \Shopware_Components_Acl $acl */
    private $acl;

    /** @var Plugin */
    private $plugin;

    /**
     * InstallManager constructor.
     *
     * @param ContainerInterface $container
     * @param InstallContext $context
     */
    public function __construct(ContainerInterface $container, InstallContext $context)
    {
        $this->container        = $container;
        $this->context          = $context;
        $this->crudService      = $this->container->get('shopware_attribute.crud_service');
        $this->modelManager     = $this->container->get('models');
        $this->acl              = $this->container->get('acl');
        $this->plugin           = $context->getPlugin();
    }

    public function install()
    {
      /*
      TODO: add models and create schema. possibly create attributes
        $this->createAttributes();
        $this->createSchema();
        $this->addACLResource();
        */
    }

    private function createAttributes()
    {
        $this->createDispatchAttributes();
        $this->createOrderBasketAttributes();
        $this->createUserAttributes();
    }

    /*
     * Create attributes for s_premium_dispatch_attributes
     *
     * @throws \Exception
     */
    private function createDispatchAttributes()
    {
        $this->crudService->update('s_premium_dispatch_attributes', 'cbs_dispatch_type', 'combobox', [
            'label' => 'Dispatch type',
            'displayInBackend' => true,
            'arrayStore' => [
                ['key' => 'collection', 'value' => 'Reservation & collection'],
                ['key' => 'delivery', 'value' => 'Payment & delivery']
            ]
        ]);
    }

    /*
     * Create attributes for s_order_basket_attributes
     *
     * @throws \Exception
     */
    private function createOrderBasketAttributes()
    {
        $this->crudService->update('s_order_basket_attributes', 'cbs_delivery_date', 'datetime', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_delivery_slot', 'integer', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_delivery_date_id', 'integer', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_delivery_zone_id', 'integer', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_delivery_vehicle_id', 'integer', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_location_id', 'string', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_collection_date', 'datetime', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_transfer_location_id', 'string', [], null, true, null);
        $this->crudService->update('s_order_basket_attributes', 'cbs_order_required', 'integer', [], null, true, null);

    }

    /*
     * Create attributes for s_user_attributes
     *
     * @throws \Exception
     */
    private function createUserAttributes()
    {
        $this->crudService->update('s_user_attributes', 'cbs_consent_purchase_specific', 'boolean', [], null, true, 0);
        $this->crudService->update('s_user_attributes', 'cbs_consent_reviews_promos', 'boolean', [], null, true, 0);
        $this->crudService->update('s_user_attributes', 'cbs_consent_first_party', 'boolean', [], null, true, 0);
    }

    private function createSchema()
    {
        $tool = new SchemaTool($this->modelManager);
        $classes = [
            $this->modelManager->getClassMetadata(DeliverySlot::class),
            $this->modelManager->getClassMetaData(DeliverySlotUsage::class),
            $this->modelManager->getClassMetaData(DeliveryZone::class),
            $this->modelManager->getClassMetaData(TransferLeadTime::class)
        ];

        foreach($classes as $class) {
            try {
                $tool->createSchema([$class]);
            } catch (\Exception $e) {
                // Ignore
            }
        }
    }

    /**
     * Adds the CBSCheckout ACL resource
     */
    private function addACLResource()
    {
        try {
            $this->acl->createResource(
                'cbs_delivery_slots',
                ['create', 'update', 'delete'],
                'CBSCheckout',
                $this->plugin->getId()
            );
        } catch (\Exception $e) {
            $this->container->get('pluginlogger')->addError($e);
        }
    }
}

<?php

namespace CBSCheckout\Bootstrap;

use CBSCheckout\Models\DeliverySlot;
use CBSCheckout\Models\DeliverySlotUsage;
use CBSCheckout\Models\DeliveryZone;
use CBSCheckout\Models\TransferLeadTime;
use Doctrine\ORM\Tools\SchemaTool;
use Shopware\Bundle\AttributeBundle\Service\CrudService;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\Context\UninstallContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Shopware\Models\Plugin\Plugin;

class UninstallManager
{
    /** @var ContainerInterface  */
    private $container;

    /** @var UninstallContext $context */
    private $context;

    /** @var ModelManager */
    private $crudService;

    /** @var CrudService */
    private $modelManager;

    /* @var \Shopware_Components_Acl $acl */
    private $acl;

    /** @var Plugin */
    private $plugin;

    /**
     * UninstallManager constructor.
     *
     * @param ContainerInterface $container
     * @param UninstallContext $context
     */
    public function __construct(ContainerInterface $container, UninstallContext $context)
    {
        $this->container        = $container;
        $this->context          = $context;
        $this->crudService      = $this->container->get('shopware_attribute.crud_service');
        $this->modelManager     = $this->container->get('models');
        $this->acl              = $this->container->get('acl');
        $this->plugin           = $context->getPlugin();
    }

    public function uninstall()
    {
      /*
      TODO: add remove and clean up code
        if ($this->context->keepUserData() === false) {
            $this->removeAttributes();
            $this->removeSchema();
            $this->removeACLResource();
        }
        */
    }

    private function removeAttributes()
    {
        $this->removeDispatchAttributes();
        $this->removeOrderBasketAttributes();
        $this->removeUserAttributes();
    }

    /*
     * Removes attributes for s_premium_dispatch_attributes
     *
     * @throws \Exception
     */
    private function removeDispatchAttributes()
    {
        $this->crudService->delete('s_premium_dispatch_attributes', 'cbs_dispatch_type', true);
    }

    /*
     * Removes attributes for s_order_basket_attributes
     *
     * @throws \Exception
     */
    private function removeOrderBasketAttributes()
    {
        $this->crudService->delete('s_order_basket_attributes', 'cbs_delivery_date', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_delivery_slot', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_delivery_zone_id', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_delivery_date_id', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_delivery_vehicle_id', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_location_id', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_collection_date', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_transfer_location_id', true);
        $this->crudService->delete('s_order_basket_attributes', 'cbs_order_required', true);

    }

    /*
    * Removes attributes for s_user_attributes
    *
    * @throws \Exception
    */
    private function removeUserAttributes()
    {
        $this->crudService->delete('s_user_attributes', 'cbs_consent_purchase_specific', true);
        $this->crudService->delete('s_user_attributes', 'cbs_consent_reviews_promos', true);
        $this->crudService->delete('s_user_attributes', 'cbs_consent_first_party', true);
    }

    private function removeSchema()
    {
        $tool = new SchemaTool($this->modelManager);
        $classes = [
            $this->modelManager->getClassMetadata(DeliverySlot::class),
            $this->modelManager->getClassMetadata(DeliverySlotUsage::class),
            $this->modelManager->getClassMetadata(DeliveryZone::class),
            $this->modelManager->getClassMetadata(TransferLeadTime::class)
        ];
        $tool->dropSchema($classes);
    }

    /**
     * Removes the CBSCheckout ACL resource
     */
    private function removeACLResource()
    {
        $this->acl->deleteResource('cbs_delivery_slots');
    }
}

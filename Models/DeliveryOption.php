<?php

namespace CBSDeliveryAreaMatrix\Models;

use Shopware\Components\Model\ModelEntity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Table(name="s_plugin_cbs_delivery_area_option")
 * @ORM\Entity(repositoryClass="Repository")
 */
class DeliveryOption extends ModelEntity
{
    /**
     * Primary Key - autoincrement value
     *
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * delivery, delivery+installation
     * @ORM\Column(name="option", type="varchar", length=20, nullable=false)
     */
    private $option;

/**
 * @return integer $id
 */
public function getId()
{
    return $this->id;
}

      /**
 * @param integer $id $id
 *
 */
public function setId(integer $id $id)
{
    $this->id = $id;

}

      /**
 * @return mixed
 */
public function getOption()
{
    return $this->option;
}

      /**
 * @param mixed $option
 *
 */
public function setOption($option)
{
    $this->option = $option;

}

      }

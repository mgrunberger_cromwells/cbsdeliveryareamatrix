<?php

namespace CBSDeliveryAreaMatrix\Models;

use Shopware\Components\Model\ModelEntity;
use Doctrine\ORM\Mapping as ORM;
//old code is here: \templatecode\test\code\classes\delivery\cls_delivery.php
/**
 * @ORM\Table(name="s_plugin_cbs_delivery_area")
 * @ORM\Entity(repositoryClass="Repository")
 */
class DeliveryArea extends ModelEntity
{
    /**
     * Primary Key - autoincrement value
     *
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="postcode", type="varchar", length=8, nullable=false)
     */
    private $postcode;

    /**TODO: set FK relationship
     * @ORM\Column(name="delivery_option_id", type="integer", length=8, nullable=false)
     */
    private $deliveryOption;


    /**
     * @ORM\Column(name="van", type="varchar", length=8, nullable=false)
     */
    private $van;


    /**
     * @ORM\Column(name="mon-all-day", type="boolean", nullable=true)
     */
    private $monAllDay;

    /**
     * @ORM\Column(name="mon-am", type="boolean", nullable=true)
     */
    private $monAm;

    /**
     * @ORM\Column(name="mon-pm", type="boolean", nullable=true)
     */
    private $monPm;

    /**
     * @ORM\Column(name="tue-all-day", type="boolean", nullable=true)
     */
    private $tueAllDay;

    /**
     * @ORM\Column(name="tue-am", type="boolean", nullable=true)
     */
    private $tueAm;

    /**
     * @ORM\Column(name="tue-pm", type="boolean", nullable=true)
     */
    private $tuePm;

    /**
     * @ORM\Column(name="wed-all-day", type="boolean", nullable=true)
     */
    private $wedAllDay;

    /**
     * @ORM\Column(name="wed-am", type="boolean", nullable=true)
     */
    private $wedAm;

    /**
     * @ORM\Column(name="wed-pm", type="boolean", nullable=true)
     */
    private $wedPm;


        /**
         * @ORM\Column(name="thu-all-day", type="boolean", nullable=true)
         */
        private $thuAllDay;

        /**
         * @ORM\Column(name="thu-am", type="boolean", nullable=true)
         */
        private $thuAm;

        /**
         * @ORM\Column(name="thu-pm", type="boolean", nullable=true)
         */
        private $thuPm;



            /**
             * @ORM\Column(name="fri-all-day", type="boolean", nullable=true)
             */
            private $friAllDay;

            /**
             * @ORM\Column(name="fri-am", type="boolean", nullable=true)
             */
            private $friAm;

            /**
             * @ORM\Column(name="fri-pm", type="boolean", nullable=true)
             */
            private $friPm;


                /**
                 * @ORM\Column(name="sat-all-day", type="boolean", nullable=true)
                 */
                private $satAllDay;

                /**
                 * @ORM\Column(name="sat-am", type="boolean", nullable=true)
                 */
                private $satAm;

                /**
                 * @ORM\Column(name="sat-pm", type="boolean", nullable=true)
                 */
                private $satPm;


                    /**
                     * @ORM\Column(name="sun-all-day", type="boolean", nullable=true)
                     */
                    private $sunAllDay;

                    /**
                     * @ORM\Column(name="sun-am", type="boolean", nullable=true)
                     */
                    private $sunAm;

                    /**
                     * @ORM\Column(name="sun-pm", type="boolean", nullable=true)
                     */
                    private $sunPm;


}

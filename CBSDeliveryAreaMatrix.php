<?php

namespace CBSDeliveryAreaMatrix;

use CBSCheckout\Bootstrap\InstallManager;
use CBSCheckout\Bootstrap\UninstallManager;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CBSDeliveryAreaMatrix extends Plugin
{
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('cbs_delivery_area_matrix.plugin_dir', $this->getPath());

        parent::build($container);
    }

    public function install(InstallContext $context)
    {
        $installer = new InstallManager(
            $this->container,
            $context
        );

        $installer->install();

        parent::install($context);
    }

    public function uninstall(UninstallContext $context)
    {
        $uninstaller = new UninstallManager(
            $this->container,
            $context
        );

        $uninstaller->uninstall();

        parent::uninstall($context);
    }
}

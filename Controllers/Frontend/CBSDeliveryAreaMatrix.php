<?php

use Shopware\Components\Model\ModelManager;

class Shopware_Controllers_Frontend_CBSDeliveryAreaMatrix extends Enlight_Controller_Action
{
    /** @var \Enlight_Components_Session_Namespace $session */
    private $session;

    /** @var ModelManager $modelManager */
    private $modelManager;
/*
TODO: all below
*/

    public function preDispatch()
    {
        // Set dependencies
        $this->session = $this->container->get('session');
        $this->userHelper = $this->container->get('cbs_checkout.user_helper');
        $this->orderHelper = $this->container->get('cbs_checkout.order_helper');
        $this->deliveryHelper = $this->container->get('cbs_checkout.delivery_helper');
        $this->basketHelper = $this->container->get('cbs_checkout.basket_helper');
        $this->locationHelper = $this->container->get('cbs_locations.location_helper');

        $this->admin = $this->container->get('Modules')->getModule('Admin');
        $this->basket = $this->container->get('Modules')->getModule('Basket');
        $this->modelManager = $this->container->get('models');

        if (in_array($this->Request()->getActionName(), [
            'ajaxStockCheck',
            'productAvailability'
        ])) {
            $this->get('plugins')->Controller()->ViewRenderer()->setNoRender();
        }

        // Check route restrictions
        $action = $this->Request()->getActionName();
        if (in_array($action, [
            'selectDispatch'
        ])) {
            // Check for customer session
            if ($this->admin->sCheckUser()) {
                $this->redirect('CBSCheckout');
                return false;
            }

            // Check for items in basket
            if (!$this->basket->sGetBasket()) {
                $this->redirect(['controller' => 'checkout', 'action' => 'cart']);
                return false;
            }
        }
    }

    /**
     * AJAX Stock Check
     *
     * @throws Exception
     * @throws SmartyException
     */
    public function ajaxStockCheckAction()
    {
        /** @var \Enlight_Template_Manager $templateManager */
        $templateManager = $this->container->get('template');

        $latitude = $this->Request()->getParam('latitude');
        $longitude = $this->Request()->getParam('longitude');
        $detailId = $this->Request()->getParam('detailId');
        $mode = $this->Request()->getParam('mode');

        switch ($mode) {
            case 'demo':
                $locations = $this->demoStockCheck(
                    $latitude,
                    $longitude,
                    [$detailId => 1]
                );
                break;
            default:
                $locations = $this->collectionStockCheck([
                    'latitude' => $latitude,
                    'longitude' => $longitude
                ], [$detailId => 1]);
                break;
        }

        $templateManager->assign([
            'locations' => $locations,
            'mode' => $mode
        ]);

        $this->Response()->setBody(json_encode([
            'html' => $templateManager->fetch('frontend/c_b_s_checkout/stock_check.tpl'),
            'data' => $locations
        ]));
    }

    /**
     * Dispatch Summary Check
     */
    public function dispatchSummaryCheckAction()
    {
        $postcode = $this->Request()->getParam('sPostcode');

        if ($this->postcodeIsValid($postcode)) {
            if ($this->Request()->getParam('cbsAddress')) {
                $addressData = $this->Request()->getParam('cbsAddress');
                $this->orderHelper->saveShippingAddress($addressData);
                $this->orderHelper->setCollectionPosition($addressData['latitude'], $addressData['longitude'], $addressData['zipcode']);
            }
        } else {
            $errors = [
                'Postcode not valid'
            ];
            $this->session->offsetSet('dispatchSummaryErrors', $errors);
            $this->orderHelper->unsetShippingAddress();
        }

        $url = $this->Front()->Router()->assemble([
            'controller' => $this->Request()->getParam('sTarget', 'checkout'),
            'action' => $this->Request()->getParam('sTargetAction', 'cart')
        ]);

        if (!empty($this->Request()->getParam('sTargetAnchor'))) {
            $url = $url . '#' . $this->Request()->getParam('sTargetAnchor');
        }
        $this->redirect($url);
    }

    /**
     * Demo Stock Check
     *
     * @param $latitude
     * @param $longitude
     * @param $products
     * @return array|\Doctrine\ORM\EntityRepository
     * @throws \Doctrine\DBAL\DBALException
     */
    private function demoStockCheck($latitude, $longitude, $products)
    {
        $locations = $this->locationHelper->getClosestDemoEnabledLocations($latitude, $longitude);

        $availability = $this->locationHelper->getStockAvailability($locations, $products, 'demo');

        if ($availability) {
            // Add availability information to locations
            foreach ($locations as &$location) {
                if ($availability[$location['demoWarehouse']]) {
                    $availableNow = false;

                    // Loop round specific product availability for this warehouse
                    foreach ($availability[$location['demoWarehouse']] as $articleDetailId => $stock) {
                        if ($stock >= $products[$articleDetailId]) {
                            $availableNow = true;
                            break;
                        }
                    }

                    $location['availability']['now'] = $availableNow;
                }
            }

            // Reorder locations - selected and not available
            $availableNow = [];
            $notAvailable = [];

            foreach ($locations as $key => $locationRecord) {
                if ($locationRecord['availability']['now']) {
                    $availableNow[$key] = $locationRecord;
                    continue;
                }

                $notAvailable[$key] = $locationRecord;
            }

            $locations = array_merge(
                $availableNow,
                $notAvailable
            );

            return $locations;
        }
    }

    /**
     * @param $coordinates
     * @param $products
     * @return mixed
     * @throws Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    private function collectionStockCheck($coordinates, $products)
    {
        $latitude = $coordinates['latitude'];
        $longitude = $coordinates['longitude'];

        $locations = $this->locationHelper->getClosestCollectionEnabledLocations($latitude, $longitude);

        $replenishmentLocationCache = [];
        $replenishmentAvailabilityCache = [];

        $availability = $this->locationHelper->getStockAvailability($locations, $products, 'collection');

        // Add availability information to locations
        foreach ($locations as &$location) {
            $availableNow = false;

            // check if collection can be fulfilled from store stock
            if ($availability[$location['collectionWarehouse']]) {
                $buffer = $location['collectionStockBuffer'] ?? 0;

                // Loop round specific product availability for this warehouse
                foreach ($availability[$location['collectionWarehouse']] as $articleDetailId => $stock) {
                    // check if available stock is greater or equal to the number required
                    if ($stock - $buffer >= $products[$articleDetailId]) {
                        $availableNow = true;
                    } else {
                        // If 1 product isn't available now, no need to check the rest
                        $availableNow = false;
                        break;
                    }
                }
            }

            // collection can't be fulfilled from store stock
            // if configured, check warehouse stock and calculate lead time
            $availableFromWarehouse = false;
            $warehouseDate = false;

            if (!$availableNow &&
                $location['collectionReplenishmentLocationId'] &&
                $location['collectionReplenishmentLeadTime']) {

                if (!$replenishmentLocationCache[$location['collectionReplenishmentLocationId']]) {
                    // build a local cache of replenishment location and availability data as most locations
                    // will use the same warehouse location for stock replenishment
                    $replenishmentLocation = $this->locationHelper->getLocation(
                        $location['collectionReplenishmentLocationId']
                    );

                    $replenishmentLocationCache[$location['collectionReplenishmentLocationId']] = $replenishmentLocation;

                    if ($replenishmentLocation) {
                        $replenishmentAvailabilityCache[$location['collectionReplenishmentLocationId']] = $this->locationHelper->getStockAvailability(
                            [$replenishmentLocationCache[$location['collectionReplenishmentLocationId']]],
                            $products,
                            'replenishment'
                        );
                    }
                }

                // get replenishment location and availability from local cache
                $replenishmentLocation = $replenishmentLocationCache[$location['collectionReplenishmentLocationId']];
                $replenishmentAvailability = $replenishmentAvailabilityCache[$location['collectionReplenishmentLocationId']];

                if ($replenishmentLocation && $replenishmentAvailability) {
                    $buffer = $replenishmentLocation['replenishmentStockBuffer'] ?? 0;

                    // Loop round specific product availability for this warehouse
                    foreach ($replenishmentAvailability[$replenishmentLocation['replenishmentWarehouse']] as $articleDetailId => $stock) {
                        // check if available stock is greater or equal to the number required
                        if ($stock - $buffer >= $products[$articleDetailId]) {
                            $availableFromWarehouse = true;
                        } else {
                            // If 1 product isn't available now, no need to check the rest
                            $availableFromWarehouse = false;
                            break;
                        }
                    }

                }

                // all products are available in this location's replenishment warehouse, calculate lead time
                if ($availableFromWarehouse) {
                    $warehouseDate = $this->locationHelper->getNextAvailableCollectionDateWithTransfer(
                        $location,
                        $replenishmentLocation
                    );
                }
            }

            $location['availability']['now'] = $availableNow;
            $location['availability']['fromWarehouse'] = $availableFromWarehouse;
            $location['availability']['fromWarehouseDate'] = $warehouseDate;
        }

        // Reorder locations - selected, immediate availability, available with transfer and not available
        $selectedLocationId = $this->orderHelper->getCollectionLocation();

        $selected = [];
        $availableNow = [];
        $availableWarehouse = [];
        $notAvailable = [];

        foreach ($locations as $key => $locationRecord) {
            if ($locationRecord['id'] == $selectedLocationId) {
                $selected[$key] = $locationRecord;
                continue;
            }
            if ($locationRecord['availability']['now']) {
                $availableNow[$key] = $locationRecord;
                continue;
            }
            if ($locationRecord['availability']['fromWarehouse']) {
                $availableWarehouse[$key] = $locationRecord;
                continue;
            }

            $notAvailable[$key] = $locationRecord;
        }

        $locations = array_merge(
            $selected,
            $availableNow,
            $availableWarehouse,
            $notAvailable
        );

        return $locations;
    }

    /**
     * Multi-stage dispatch method
     *
     * Stage 1: address
     * Stage 2: dispatch selection
     * Stage 3: C&C storefinder/delivery picker
     */
    public function dispatchAction()
    {
        $stage = 1;

        $shippingAddress = $this->orderHelper->getShippingAddress();

        if (!empty($shippingAddress)) {
            $stage = 2;

            $availabilitySummary = $this->orderHelper->getSummaryAvailability();

            $this->View()->assign([
                'availabilitySummary' => $availabilitySummary
            ]);
        }

        $dispatch = $this->orderHelper->getDispatch();
        if ($stage == 2 && $dispatch) {
            $stage = 3;

            $this->View()->assign([
                'dispatch' => $dispatch
            ]);
        }

        if ($stage == 3) {
            if ($dispatch['cbs_dispatch_type'] == 'collection') {
                $this->handleCollectionDispatch();
            } else if ($dispatch['cbs_dispatch_type'] == 'delivery') {
                $this->handleDeliveryDispatch();
            }
        }

        $collectionPosition = $this->orderHelper->getCollectionPosition();

        $this->View()->assign([
            'dispatchStage' => $stage,
            'shippingAddress' => $shippingAddress,
            'collectionPosition' => $collectionPosition
        ]);
    }

    public function handleCollectionDispatch()
    {
        $products = [];

        if ($this->Request()->has('search-address')) {
            $this->orderHelper->setCollectionPosition($this->Request()->get('latitude'),  $this->Request()->get('longitude'), $this->Request()->get('search-address'));
        }

        foreach ($this->basket->sGetBasket()['content'] as $item) {
            if ($item['modus'] == 0) {
                $products[$item['articleDetailId']] = (int)$item['quantity'];
            }
        }

        $locations = $this->collectionStockCheck($this->orderHelper->getCollectionPosition(), $products);

        $this->View()->assign('collectionLocations', $locations);
    }

    public function handleDeliveryDispatch()
    {
        $slots = $this->deliveryHelper->getDeliverySlots();
        $selectedSlot = $this->basketHelper->getDeliveryDetails();

        $orderRequired = false;

        foreach ($this->basket->sGetBasket()['content'] as $item) {
            if ($item['modus'] == 0) {
                $orderRequired = (bool)$item['cbs_order_required'];
                if ($orderRequired === true) {
                    break;
                }
            }
        }

        $this->View()->assign([
            'deliveryDates' => $slots['dates'],
            'deliveryAvailability' => $slots['availability'],
            'selectedSlot' => $selectedSlot,
            'orderRequired' => $orderRequired
        ]);
    }

    public function dispatchConfirmationAction()
    {
        $dispatch = $this->orderHelper->getDispatch();
        if (!$dispatch) {
            $this->redirect(['controller' => 'checkout', 'action' => 'cart']);
            return;
        }

        $modules = $this->container->get('Modules');

        /** @var \sAdmin $adminModule */
        $adminModule = $modules->getModule('Admin');
        if ($adminModule->sCheckUser()) {
            $userData = $adminModule->sGetUserData();
            $this->userHelper->updateAddress($userData['additional']['user']['id']);
        }

        switch ($dispatch['cbs_dispatch_type']) {
            case 'collection':
                $this->saveCollection();
                break;
            case 'delivery':
                $this->saveDelivery();
                break;
        }

        // Redirect back to standard Shopware checkout
        $this->redirect([
            'controller' => 'checkout',
            'action' => 'confirm'
        ]);
    }

    /**
     * Store shipping address model in session for use later
     */
    public function saveShippingAddressAction()
    {
        $addressData = $this->Request()->getParam('cbsAddress');

        $this->orderHelper->saveShippingAddress($addressData);

        $url = $this->container->get('router')->assemble([
            'controller' => 'CBSCheckout',
            'action' => 'dispatch'
        ]);

        $this->redirect($url);
    }

    /**
     * Save selected dispatch method
     */
    public function setDispatchMethodAction()
    {
        $dispatch = $this->Request()->getParam('dispatch')[0];

        if (!$dispatch) {
            $this->redirect(['controller' => 'checkout', 'action' => 'cart']);
        }

        $this->orderHelper->setDispatchByType($dispatch);

        $url = $this->Front()->Router()->assemble([
            'controller' => $this->Request()->getParam('sTarget', 'CBSCheckout'),
            'action' => $this->Request()->getParam('sTargetAction', 'dispatch')
        ]);

        if (!empty($this->Request()->getParam('sTargetAnchor'))) {
            $url = $url . '#' . $this->Request()->getParam('sTargetAnchor');
        }

        $this->redirect($url);
    }

    /**
     * Product Availability Check
     *
     * @return bool
     * @throws Exception
     */
    public function productAvailabilityAction()
    {
        $rawPost = $this->Request()->getRawBody();
        $ids = json_decode($rawPost);
        $availability = [];

        if (!empty($ids)) {
            /** @var sArticles $articlesModule */
            $articlesModule = $this->container->get('Modules')->Articles();

            foreach ($ids as $id) {
                $article = $articlesModule->sGetArticleById($id);
                $this->View()->sArticle = $article;
                $template = $this->View()->fetch('frontend/listing/click_collect.tpl');
                $availability[$id] = $template;
            }
        }

        $this->Response()->setBody(json_encode([
            'result' => true,
            'data' => $availability
        ]));

        return true;
    }

    public function setCollectionPosition()
    {
        $latitude = $this->Request()->getParam('latitude');
        $longitude = $this->Request()->getParam('longitude');

        $this->orderHelper->setCollectionPosition($latitude, $longitude);

        $this->redirect([
            'controller' => $this->Request()->getParam('sTarget', 'CBSCheckout'),
            'action' => $this->Request()->getParam('sTargetAction', 'dispatch')
        ]);
    }

    /**
     * @param $postcode
     * @return bool
     */
    private function postcodeIsValid($postcode)
    {
        return true;
        return !!preg_match('/^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$/i', $postcode);
    }

    /**
     * Save details for collection against basket attribtues
     */
    private function saveCollection()
    {
        $locationId = $this->Request()->get('locationId');
        $warehouseId = $this->Request()->get('warehouseId');
        $location = $this->locationHelper->getLocation($locationId);
        $collectionDate = null;
        $transferLocationId = null;

        // Warehouse ID is only set if this collection needs a transfer from a warehouse
        if ($warehouseId) {
            $transferLocationId = $warehouseId;
            $warehouse = $this->locationHelper->getLocation($warehouseId);

            $collectionDate = $this->locationHelper->getNextAvailableCollectionDateWithTransfer(
                $location,
                $warehouse
            );
        } else {
            $collectionDate = $this->locationHelper->getNextAvailableCollectionDate($location);
        }

        $this->basketHelper->updateBasketAttributes([
            'cbs_location_id' => $locationId,
            'cbs_collection_date' => $collectionDate['date'],
            'cbs_transfer_location_id' => $transferLocationId,
            'cbs_delivery_date' => null,
            'cbs_delivery_slot' => null,
            'cbs_delivery_zone_id' => null,
            'cbs_delivery_date_id' => null,
            'cbs_delivery_vehicle_id' => null
        ]);
    }

    /**
     * Save details for delivery against basket attributes
     */
    private function saveDelivery()
    {
        $deliveryDate = $this->Request()->get('deliveryDate');
        $deliverySlot = $this->Request()->get('deliverySlot');
        $deliveryDateId = $this->Request()->get('deliveryDateId');

        // Convert to DateTime object
        $deliveryDate = DateTime::createFromFormat('Y-m-d', $deliveryDate);

        if ($deliveryDate instanceof DateTime) {
            $deliveryDate = $deliveryDate->format('Y-m-d G:i:s');
        }

        $this->basketHelper->updateBasketAttributes([
            'cbs_location_id' => null,
            'cbs_collection_date' => null,
            'cbs_delivery_date' => $deliveryDate,
            'cbs_delivery_slot' => $deliverySlot,
            'cbs_delivery_date_id' => $deliveryDateId
        ]);
    }
}

<?php

class Shopware_Controllers_Backend_CBSDeliveryAreaMatrix extends Shopware_Controllers_Backend_Application
{
    protected $model = 'CBSDeliveryAreaMatrix\Models\DeliveryArea';
    protected $alias = 'deliveryarea';
/* TODO: all below
    public function save()
    {

        $params = $this->Request()->getParams();

        if ($params['timeFrom']) {
            $timeFrom = new Zend_Date();
            $timeFrom->set($params['timeFrom'], Zend_Date::TIME_SHORT);
            $timeFrom = $timeFrom->get(Zend_Date::MINUTE) * 60 + $timeFrom->get(Zend_Date::HOUR) * 60 * 60;
            $params['timeFrom'] = $timeFrom;
        }

        if ($params['timeTo']) {
            $timeTo = new Zend_Date();
            $timeTo->set($params['timeTo'], Zend_Date::TIME_SHORT);
            $timeTo = $timeTo->get(Zend_Date::MINUTE) * 60 + $timeTo->get(Zend_Date::HOUR) * 60 * 60;
            $params['timeTo'] = $timeTo;
        }

        return parent::save($params);

    }

    public function updateSlotPositionAction()
    {
        $db = $this->container->get('db');

        $slots = json_decode($this->Request()->getParam('data'));

        foreach ($slots as $position => $slotId) {
            $db->update(
                's_plugin_cbs_delivery_slot',
                ['position' => $position],
                'id = ' . $slotId
            );
        }

        $this->View()->assign(['success' => true]);
    }

    public function getListQuery()
    {
        $builder = parent::getListQuery();

        $builder->addOrderBy('deliveryslot.position');

        return $builder;
    }

    protected function getList($offset, $limit, $sort = [], $filter = [], array $wholeParams = [])
    {
        $list = parent::getList($offset, $limit, $sort, $filter, $wholeParams);

        foreach ($list['data'] as &$data) {
            if ($data['timeFrom']) {
                $data['timeFrom'] = gmdate('H:i', $data['timeFrom']);
            }
            if ($data['timeTo']) {
                $data['timeTo'] = gmdate('H:i', $data['timeTo']);
            }
        }

        return ['success' => true, 'data' => $list['data'], 'total' => $list['count']];
    }
*/
}

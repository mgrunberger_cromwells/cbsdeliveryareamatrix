<?php

namespace CBSDeliveryAreaMatrix\Components;

use CBSDeliveryAreaMatrix\Models\DeliveryArea;
use Shopware\Components\Model\ModelManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DeliveryArea
{
    /** @var ContainerInterface $container */
    private $container;

    /** @var ModelManager $modelManager */
    private $modelManager;

  public function __construct(ContainerInterface $container, ModelManager $modelManager)
    {
        $this->container = $container;
        $this->modelManager = $modelManager;

    }

    /**
     * Function to get a delivery slot by slot id
     *
     * @param $slotId
     * @return mixed
     */
    public function getDeliverySlotsByPostcode ($postcode)
    {
      /*TODO: write query: select * from delivery_area where postcode = $postcode??
        $slotsBuilder = $this->modelManager->getDBALQueryBuilder();
        $slotsTable = $this->modelManager->getClassMetadata(DeliveryArea::class)->getTableName();
        $slotsBuilder->select('slots.*')
            ->from($slotsTable, 'slots')
            ->andWhere('slots.id = ?')
            ->setParameter(0, $slotId);

        return $slotsBuilder->execute()->fetch();
    }




//TODO: could be useful...
    /**
     * Break down postcode into required format
     *
     * @param $postcode
     * @return array
     */
    private function formatPostcode($postcode)
    {
        preg_match('/([a-z]{1,2})(\d+)\s(\d+)([a-z]+)/i', $postcode, $parts);

        return [
            'area' => $parts[1],
            'district' => $parts[2],
            'sector' => $parts[3],
            'unit' => $parts[4]
        ];
    }

/*
  TODO: from old site
*/
    public function oldformatPostcode($postcode){

  		$postcode = str_replace(' ', '', strtoupper($postcode) );

          if (strlen($postcode) == 5) {

              // Format postcodes such as B1 1LS
              $postcode = substr($postcode, 0, 2) . ' ' . substr($postcode, 2);

          } elseif(strlen($postcode) == 6){

              // Format postcodes such as GL1 1AD
  			$postcode = substr($postcode, 0, 3) . ' ' . substr($postcode, 3);

  		} elseif(strlen($postcode) == 7){

              // Format postcodes such as GL27 1AD or WC1A 1AP
  			$postcode = substr($postcode, 0, 4) . ' ' . substr($postcode, 4);

  		} else {

  			$postcode = false;

  		}

  		return $postcode;

  	}


}

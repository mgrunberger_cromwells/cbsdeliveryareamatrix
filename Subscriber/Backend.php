<?php

namespace CBSDeliveryAreaMatrix\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Backend implements SubscriberInterface
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

/*
TODO: TBD what this should do
*/
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_CBSDeliveryAreaMatrix' => 'onGetAreasControllerPath'
        ];
    }

    public function onGetAreasControllerPath(\Enlight_Event_EventArgs $args)
    {
        $this->container->get('template')->addTemplateDir(__DIR__ . '/..' . '/Resources/views/');

        return __DIR__ . '/../Controllers/Backend/CBSDeliveryArea.php';
    }

}

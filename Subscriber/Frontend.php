<?php

namespace CBSDeliveryAreaMatrix\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Components\Plugin\CachedConfigReader;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Frontend implements SubscriberInterface
{
    /** @var ContainerInterface $container */
    private $container;

    /** @var CachedConfigReader $configReader */
    private $configReader;

    /**
     * Checkout constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, CachedConfigReader $configReader)
    {
        $this->container = $container;
        $this->configReader = $configReader;
    }
    /*
    TODO: TBD what this should do
    */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend' => 'onFrontendPostDispatch'
        ];
    }

    /**
     * Add Templates & Snippets to view
     *
     * @param \Enlight_Event_EventArgs $args
     */
    public function onFrontendPostDispatch(\Enlight_Event_EventArgs $args)
    {
        $config = $this->configReader->getByPluginName('CBSDeliveryAreaMatrix');
        // Add Templates
        $this->container->get('template')->addTemplateDir(
            $this->getPluginPath() . '/Resources/views'
        );
    }

    /**
     * @return string
     */
    private function getPluginPath()
    {
        return $this->container->getParameter('cbs_delivery_area_matrix.plugin_dir');
    }
}
